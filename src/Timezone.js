export default class Timezone {
    constructor(timezoneName, nickName, boundaryStart, start, end, boundaryEnd) {
        this.timezoneName = timezoneName;
        this.nickName = nickName;
        this.boundaryStart = boundaryStart;
        this.start = start;
        this.end = end;
        this.boundaryEnd = boundaryEnd;
    }

    static defaultFor(timezoneName) {
        return new Timezone(timezoneName, timezoneName, '08:00', '09:00', '18:00', '19:00');
    }

    toCSV() {
        return `${this.timezoneName},${this.nickName},${this.boundaryStart},${this.start},${this.end},${this.boundaryEnd}`;
    }
}
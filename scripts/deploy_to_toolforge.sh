#!/usr/bin/env bash

set -e

user=${1-$USER}
project="tztables"

npm install
npm run build
scp -r dist $user@login.toolforge.org:/data/project/${project}/dist
ssh $user@login.toolforge.org "
  set -ex
  become ${project} bash -xc \"
    take dist &&
    set -e &&
    rm -rf public_html.bak &&
    mv public_html public_html.bak &&
    mv dist public_html &&
    webservice stop &&
    webservice start
  \"
"
